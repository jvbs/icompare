<?php
/** 
 * Model que representa uma pergunta
 */
namespace Models;

class Smartphones extends BaseModel {
  protected $tableName = 'celulares';

  protected $id;
  protected $marca;
  protected $modelo;
  protected $geral;
  protected $hardware;
  protected $camera;
  protected $bateria;
  protected $tela;
  protected $sensores;
  protected $umidade;
  protected $status;

  /**
   * Sobrescreve o método find da BaseModel, para definir a propriedade "user", com o objeto \Models\User correspondente
   */
  public function find($value, $field = 'id', $fieldType = \PDO::PARAM_STR){
    parent::find($value, $field, $fieldType);

    $this->user = new \Models\User;
    $this->user->find($this->user_id);
  }

  public static function all(){
    $DB = new \DB;
    $sql = "SELECT * FROM celulares where status = 1";
    $stmt = $DB->prepare($sql);
    $stmt->execute();

    $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
    return $rows;
  }


  /**
   * Gets the value of id.
   *
   * @return int
   */
  public function getId(){
    return $this->id;
  }

  /**
   * Sets the value of id.
   *
   * @param int $id the id
   */
  protected function setId($id){
    $this->id = $id;
  }
}