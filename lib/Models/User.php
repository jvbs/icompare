<?php
/**
 * Model de Usuarios
 */
namespace Models;

class User extends BaseModel {
  const STATUS_ACTIVE = 1;
  const STATUS_INACTIVE = 2;

  protected $tableName = 'usuarios';
  
  protected $id;
  protected $nome;
  protected $data_nascimento;
  protected $email;
  protected $senha;
  protected $token;
  protected $status;
  protected $admin;
  protected $imagem_perfil;
  protected $criado_em;
  protected $atualizado_em;

  /**
   * Gera o token que será salvo no cookie
   */
  public function generateToken(){
    $string = sprintf("%d%s%s", $this->id, $this->senha, microtime(true));
    $token = md5($string);
    $this->updateToken($token);

    return $token;
  }

  /**
   * Atualiza o token de acesso, no próprio objeto e no banco de dados
   */
  public function updateToken($token){
    $this->token = $token;
    $now = date('Y-m-d H:i:s');
    $DB = new \DB;
    $sql = "UPDATE usuarios SET token = :token, atualizado_em = :now WHERE id = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(":token", $token);
    $stmt->bindParam(":now", $now);
    $stmt->bindParam(":id", $this->id, \PDO::PARAM_INT);
    $stmt->execute();
  }

  /**
   * Altera a senha do usuário
   */
  public function changePassword($password){
    $hashedNewPassword = \Hash::hash_pass($password);
    $this->password = $hashedNewPassword;
    $userID = $this->getId();
    $now = date('Y-m-d H:i:s');
    $DB = new \DB;
    $sql = "UPDATE users SET password = :password, updated_at = :now WHERE id = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':password', $hashedNewPassword);
    $stmt->bindParam(':now', $now);
    $stmt->bindParam(':id', $userID, \PDO::PARAM_INT);
    if($stmt->execute()){
      // gera um novo token
      $token = $this->generateToken();
      $this->updateToken($token);

      // atualiza o token do cookie
      \Controllers\SessionsController::saveSessionCookieForUser($this);

      return true;
    } else {
      return false;
    }
  }

  /**
   * Gets the value of nickname.
   */
  public function getNome(){
    return $this->nome;
  }

  /**
   * Sets the value of nickname.
   */
  public function setNome($nome){
    $this->nome = $nome;
  }

  /**
   * Gets the value of email.
   */
  public function getEmail(){
    return $this->email;
  }

  /**
   * Sets the value of email.
   */
  private function _setEmail($email){
    $this->email = $email;
  }

  /**
   * Gets the value of token.
   */
  public function getToken(){
    return $this->token;
  }

  /**
   * Sets the value of token.
   */
  private function _setToken($token){
    $this->token = $token;
  }

  /**
   * Gets the value of admin.
   */
  public function isAdmin(){
    return (bool) $this->admin;
  }

  /**
   * Sets the value of admin.
   */
  private function _setAdmin($admin){
    $this->admin = $admin == 1 ? true : false;
  }

  /**
   * Gets the value of created_at.
   */
  public function getCriadoEm(){
    return $this->criado_em;
  }

  /**
   * Sets the value of criado_em.
   */
  private function _setCriadoEm(DateTime $criado_em){
    $this->criado_em = $criado_em;
  }

  /**
   * Gets the value of atualizado_em.
   */
  public function getAtualizadoEm(){
    return $this->atualizado_em;
  }

  /**
   * Sets the value of atualizado_em.
   */
  private function _setAtualizadoEm(DateTime $atualizado_em){
    $this->atualizado_em = $atualizado_em;
  }

  /**
   * Gets the value of id.
   */
  public function getId(){
    return $this->id;
  }

  /**
   * Sets the value of id.
   */
  private function _setId($id){
    $this->id = (int) $id;
  }

  public function getSenha(){
    return $this->senha;
  }

  /**
   * Gets the value of status.
   */
  public function getStatus(){
    return $this->status;
  }

  /**
   * Sets the value of status.
   */
  private function _setStatus($status){
    $this->status = (int) $status;
  }
}