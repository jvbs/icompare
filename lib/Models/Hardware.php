<?php
/** 
 * Model que representa uma pergunta
 */
namespace Models;

class Hardware extends BaseModel {
  protected $tableName = 'hardware';

  protected $id;
  protected $user_id;
  public $user; // \Models\User object
  protected $nucleos_cpu;
  protected $memoria_ram;
  protected $armazenamento_interno;
  protected $cartao_memoria;
  protected $status;

  /**
   * Sobrescreve o método find da BaseModel, para definir a propriedade "user", com o objeto \Models\User correspondente
   */
  public function find($value, $field = 'id', $fieldType = \PDO::PARAM_STR){
    parent::find($value, $field, $fieldType);

    $this->user = new \Models\User;
    $this->user->find($this->user_id);
  }

  public static function all(){
    $DB = new \DB;
    $sql = "SELECT * FROM hardware where status = 1";
    $stmt = $DB->prepare($sql);
    $stmt->execute();

    $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
    return $rows;
  }


  /**
   * Gets the value of id.
   *
   * @return int
   */
  public function getId(){
    return $this->id;
  }

  /**
   * Sets the value of id.
   *
   * @param int $id the id
   */
  protected function setId($id){
    $this->id = $id;
  }

  /**
   * Gets the value of user_id.
   *
   * @return int
   */
  public function getUserId(){
    return $this->user_id;
  }

  /**
   * Sets the value of user_id.
   *
   * @param int $user_id the user id
   */
  protected function setUserId($user_id){
    $this->user_id = $user_id;
  }
}