<?php
/**
 * Criptografando a senha - BCrypt
*/

class Hash {
  public static function hash_pass($senha){
    $opcoes = [
      'cost' => 10
    ];

    return password_hash($senha, PASSWORD_DEFAULT, $opcoes);
  }
}