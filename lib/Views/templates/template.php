<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>iCompare</title>
  <!-- Import Google Icon Font -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- Materalize 1.0.0 -->
  <link href="assets/css/materialize/index.min.css" rel="stylesheet">
  <!-- Project -->
  <link href="assets/css/style.css" rel="stylesheet">
  <!-- jQuery 3.4.1 -->
  <script src="assets/js/jquery/index.min.js"></script>
  <!-- Materialize 1.0.0 -->
  <script src="assets/js/materialize/index.min.js"></script>
  <!-- Init project -->
  <script src="assets/js/init.js"></script>
  <!-- Sweet Alert -->
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>
<body>
<header class="header">
  <section class="navbar">
  <nav class="deep-purple accent-3">
    <div class="nav-wrapper">
      <a href="<?=getBaseURL()?>" class="brand-logo center">
        <img class="responsive-img" id="logo-img" src="assets/img/logo_icompare_white.svg" alt="Logo iCompare" style="width">
      </a>
      <a href="#" data-target="mobile-menu" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <?php if( ( $user = Auth::user() ) != null ): ?>
          <li><a href="#"><i class="material-icons left">person</i> Olá, <?=$user->getNome();?></a></li>
          <li><a href="<?=getBaseURL();?>/logout"><i class="material-icons left">power_settings_new</i> Sair</a></li>
        <?php else: ?>
          <li><a href="<?=getBaseURL();?>/login"><i class="material-icons left">exit_to_app</i>Login</a></li>
        <?php endif; ?>
      </ul>
    </div>
  </nav>

  <ul class="sidenav" id="mobile-menu">
    <?php if( ( $user = Auth::user() ) != null ): ?>
      <li><a class="text-center"><?=$user->getNome();?></a></li>
      <li><a href="<?=getBaseURL();?>/logout"><i class="material-icons left">power_settings_new</i> Sair</a></li>
    <?php else: ?>
      <li><a href="<?=getBaseURL();?>/login"><i class="material-icons left">exit_to_app</i>Login</a></li>
    <?php endif; ?>
  </ul>
  </section>
</header>

<main>
<?php
  if(isset($view)){
    $path = viewsPath().$view.'.php';
    if(file_exists($path)){
      require_once $path;
    }
  } else {
    echo 'View não encontrada :(';
  }
  ?>
</main>
</body>
<script>
$("select[required]").css({display: "block", height: 0, padding: 0, width: 0, position: 'absolute'});
</script>
</html>