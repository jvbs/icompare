<div class="container center-align" style="margin-top:25px">
  <div class="row">
    <div class="col m6 s6 center">
      <div class="celular-select">
        <img class="celular-comparativo" src="assets/img/phone.svg" alt="Celular 1">
        <select name="celular_1" id="celular_1" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <?php foreach($celulares as $celular){
            echo "<option value='{$celular->id}'>{$celular->marca} {$celular->modelo}</option>";
          }
          ?>
        </select>
      </div>
    </div>
    <div class="col m6 s6 center">
      <div class="celular-select">
        <img class="celular-comparativo" src="assets/img/phone.svg" alt="Celular 2">
        <select name="celular_2" id="celular_2" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <?php foreach($celulares as $celular){
            echo "<option value='{$celular->id}'>{$celular->marca} {$celular->modelo}</option>";
          }
          ?>
        </select>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col m12">
      <div class="field center">
        <button class="btn deep-purple accent-3" id="comparar">Comparar</button>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col m6 s6">
      <table class="striped centered" id="tab_celular_1">
        <tbody id="tbody1"></tbody>
      </table>
    </div>
    <div class="col m6 s6">
      <table class="striped centered" id="tab_celular_2">
        <tbody id="tbody2"></tbody>
      </table>
    </div>
</div>
<script>
$("#comparar").click(function(e){
  e.preventDefault();
  // validacao dos campos
  var celular_1 = $("#celular_1").val()
  var celular_2 = $("#celular_2").val()
  // caso ambos estejam preenchidos
  if(celular_1 && celular_2){
    $.post("<?=getBaseURL()?>/comparar", { celular_1: celular_1, celular_2: celular_2 })
      .done(function(data){
        var celulares = JSON.parse(data)
        $("#tbody1, #tbody2").remove()
        html = ""
        html += "<tbody id='tbody1'>"
        html +="<tr>"
        html += "      <td colspan=2><strong>Dispositivo</strong></td>"
        html += "    </tr>"
        html += "  <tr>"
        html += "    <td>Marca</td>"
        html += "    <td>"+celulares['celular_1'][0]['marca']+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Modelo</td>"
        html += "    <td>"+celulares['celular_1'][0]['modelo']+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td colspan=2><strong>Geral</strong></td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Sistema operacional</td>"
        html += "    <td>"+celulares['celular_1'][0]['sistema']+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Versão do sistema operacional</td>"
        html += "    <td>"+celulares['celular_1'][0]['versao_sistema']+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td colspan=2><strong>Hardware</strong></td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Núcleos CPU</td>"
        html += "    <td>"+celulares['celular_1'][0]['nucleos_cpu']+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Memória RAM</td>"
        html += "    <td>"+celulares['celular_1'][0]['memoria_ram']+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Armazenamento Interno</td>"
        html += "    <td>"+celulares['celular_1'][0]['armazenamento_interno']+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Cartão de Memória</td>"
        html += "    <td>"+((celulares['celular_1'][0]['cartao_memoria']) == 1 ? "Sim" : "Não")+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td colspan=2><strong>Câmera</strong></td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Resolução Traseira</td>"
        html += "    <td>"+celulares['celular_1'][0]['resolucao_traseira']+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Resolução Frontal</td>"
        html += "    <td>"+celulares['celular_1'][0]['resolucao_frontal']+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td colspan=2><strong>Bateria</strong></td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Tipo</td>"
        html += "    <td>"+celulares['celular_1'][0]['tipo_bateria']+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Capacidade (MpH)</td>"
        html += "    <td>"+celulares['celular_1'][0]['capacidade']+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Carregamento sem fio</td>"
        html += "    <td>"+((celulares['celular_1'][0]['carregamento_semfio']) == 1 ? "Sim" : "Não")+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td colspan=2><strong>Tela</strong></td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Tipo</td>"
        html += "    <td>"+celulares['celular_1'][0]['tipo_tela']+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Resolução</td>"
        html += "    <td>"+celulares['celular_1'][0]['resolucao']+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Densidade de Pixels (PPI)</td>"
        html += "    <td>"+celulares['celular_1'][0]['densidade_pixel']+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td colspan=2><strong>Sensores</strong></td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Acelerômetro</td>"
        html += "    <td>"+((celulares['celular_1'][0]['acelerometro']) == 1 ? "Sim" : "Não")+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Barômetro</td>"
        html += "    <td>"+((celulares['celular_1'][0]['barometro']) == 1 ? "Sim" : "Não")+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Batimentos Cardíacos</td>"
        html += "    <td>"+((celulares['celular_1'][0]['batimentos_cardiacos']) == 1 ? "Sim" : "Não")+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Bússola</td>"
        html += "    <td>"+((celulares['celular_1'][0]['bussola']) == 1 ? "Sim" : "Não")+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Giroscópio</td>"
        html += "    <td>"+((celulares['celular_1'][0]['giroscopio']) == 1 ? "Sim" : "Não")+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Impressão Digital</td>"
        html += "    <td>"+((celulares['celular_1'][0]['impressao_digital']) == 1 ? "Sim" : "Não")+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Proximidade</td>"
        html += "    <td>"+((celulares['celular_1'][0]['proximidade']) == 1 ? "Sim" : "Não")+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Temperatura</td>"
        html += "    <td>"+((celulares['celular_1'][0]['temperatura']) == 1 ? "Sim" : "Não")+"</td>"
        html += "  </tr>"
        html += "  <tr>"
        html += "    <td>Umidade</td>"
        html += "    <td>"+((celulares['celular_1'][0]['umidade']) == 1 ? "Sim" : "Não")+"</td>"
        html += "  </tr>"
        html += "</tbody>"



        html2 = ""
        html2 += "<tbody id='tbody2'>"
        html2 +="<tr>"
        html2 += "      <td colspan=2><strong>Dispositivo</strong></td>"
        html2 += "    </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Marca</td>"
        html2 += "    <td>"+celulares['celular_2'][0]['marca']+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Modelo</td>"
        html2 += "    <td>"+celulares['celular_2'][0]['modelo']+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td colspan=2><strong>Geral</strong></td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Sistema operacional</td>"
        html2 += "    <td>"+celulares['celular_2'][0]['sistema']+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Versão do sistema operacional</td>"
        html2 += "    <td>"+celulares['celular_2'][0]['versao_sistema']+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td colspan=2><strong>Hardware</strong></td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Núcleos CPU</td>"
        html2 += "    <td>"+celulares['celular_2'][0]['nucleos_cpu']+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Memória RAM</td>"
        html2 += "    <td>"+celulares['celular_2'][0]['memoria_ram']+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Armazenamento Interno</td>"
        html2 += "    <td>"+celulares['celular_2'][0]['armazenamento_interno']+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Cartão de Memória</td>"
        html2 += "    <td>"+((celulares['celular_2'][0]['cartao_memoria']) == 1 ? "Sim" : "Não")+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td colspan=2><strong>Câmera</strong></td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Resolução Traseira</td>"
        html2 += "    <td>"+celulares['celular_2'][0]['resolucao_traseira']+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Resolução Frontal</td>"
        html2 += "    <td>"+celulares['celular_2'][0]['resolucao_frontal']+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td colspan=2><strong>Bateria</strong></td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Tipo</td>"
        html2 += "    <td>"+celulares['celular_2'][0]['tipo_bateria']+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Capacidade (MpH)</td>"
        html2 += "    <td>"+celulares['celular_2'][0]['capacidade']+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Carregamento sem fio</td>"
        html2 += "    <td>"+((celulares['celular_2'][0]['carregamento_semfio']) == 1 ? "Sim" : "Não")+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td colspan=2><strong>Tela</strong></td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Tipo</td>"
        html2 += "    <td>"+celulares['celular_2'][0]['tipo_tela']+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Resolução</td>"
        html2 += "    <td>"+celulares['celular_2'][0]['resolucao']+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Densidade de Pixels (PPI)</td>"
        html2 += "    <td>"+celulares['celular_2'][0]['densidade_pixel']+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td colspan=2><strong>Sensores</strong></td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Acelerômetro</td>"
        html2 += "    <td>"+((celulares['celular_2'][0]['acelerometro']) == 1 ? "Sim" : "Não")+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Barômetro</td>"
        html2 += "    <td>"+((celulares['celular_2'][0]['barometro']) == 1 ? "Sim" : "Não")+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Batimentos Cardíacos</td>"
        html2 += "    <td>"+((celulares['celular_2'][0]['batimentos_cardiacos']) == 1 ? "Sim" : "Não")+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Bússola</td>"
        html2 += "    <td>"+((celulares['celular_2'][0]['bussola']) == 1 ? "Sim" : "Não")+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Giroscópio</td>"
        html2 += "    <td>"+((celulares['celular_2'][0]['giroscopio']) == 1 ? "Sim" : "Não")+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Impressão Digital</td>"
        html2 += "    <td>"+((celulares['celular_2'][0]['impressao_digital']) == 1 ? "Sim" : "Não")+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Proximidade</td>"
        html2 += "    <td>"+((celulares['celular_2'][0]['proximidade']) == 1 ? "Sim" : "Não")+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Temperatura</td>"
        html2 += "    <td>"+((celulares['celular_2'][0]['temperatura']) == 1 ? "Sim" : "Não")+"</td>"
        html2 += "  </tr>"
        html2 += "  <tr>"
        html2 += "    <td>Umidade</td>"
        html2 += "    <td>"+((celulares['celular_2'][0]['umidade']) == 1 ? "Sim" : "Não")+"</td>"
        html2 += "  </tr>"
        html2 += "</tbody>"
        $("#tab_celular_1").append(html)
        $("#tab_celular_2").append(html2)
        
      })
      .fail(function(err, errDtl, errThrown){
        Swal.fire({
          title: 'Ops!',
          text: 'Aconteceu um erro no processamento. Tente novamente mais tarde!',
          icon: 'error',
          confirmButtonText: 'Ok'
        })
      })
  } else {
    Swal.fire({
      title: 'Ei ei ei!',
      text: 'Por favor selecione os dois celulares para realizar o comparativo!',
      icon: 'warning',
      confirmButtonText: 'Ok'
    })
  }
})
</script>