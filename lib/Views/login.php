<style>
.login {
  max-width: 400px;
  margin-top: 60px;
}
.login h2 {
  font-size: 2.4em;
}
.login .field {
  margin-bottom: 16px;
}
</style>
<div class="login container">
  <form action="<?=getBaseURL()?>/login" method="POST" class="card-panel">
    <h2 class="center deep-purple-text text-accent-3">Login</h2>
    <div class="input-field">
      <label for="email">Email</label>
      <input type="email" name="email">
    </div>
    <div class="input-field">
    <label for="password">Senha</label>
      <input type="password" name="password">
      <?php if(isset($errors) && count($errors) > 0): ?>
        <div class="row">
          <div class="card red white-text darken-1">
            <ul>
                <?php foreach($errors as $error) : ?>
                <li><?=$error?></li>
                <?php endforeach; ?>
            </ul>
          </div>
        </div>
      <?php endif; ?>       
      <!-- <span class="helper-text" data-error="wrong" data-success="right">No mínimo 6 caracteres.</span> -->
      </div>
    <div class="field">
      <button class="btn deep-purple accent-3">Login</button>
    </div>
  </form>
</div>