<div class="container">
  <div class="row">
    <div class="page-header">
      <h1>Editar Bateria</h1>
    </div>
  </div>
  <?php if(isset($errors) && count($errors) > 0): ?>
  <div class="card red darken-1">
    <div class="row">
      <div class="card-content white-text">
        <?php foreach($errors as $chave => $error): ?>
          <p>
            <?=($chave+1).'. '.$error;?>
          </p>
        <?php endforeach; ?>
      </div>
    </div>
   </div>
  <?php endif; ?>
  <div class="row">
    <form action="<?=getBaseURL()?>/atualizar-bateria" method="POST">
      <div class="input-field">
        <input type="text" id="id_campo" name="id_campo" readonly="readonly">
      </div>
      <div class="input-field">
        <select name="tipo" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="Li-Po">Li-Po</option>
          <option value="Ni-Cd">Ni-Cd</option>
          <option value="Ni-MH">Ni-MH</option>
        </select>
        <label>Tipo de Bateria</label>
      </div>
      <div class="input-field">
        <input type="number" name="capacidade" required>
        <label>Capacidade</label>
      </div>
      <div class="input-field">
        <select name="carregamento_semfio" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="Sim">Sim</option>
          <option value="Não">Não</option>
        </select>
        <label>Carregamento sem fio</label>
      </div>
      <div class="field">
        <button class="btn deep-purple accent-3 right">Salvar</button>
      </div>
    </form>
  </div>
</div>