<div class="row" style="margin:10px 25px">
  <a href="<?=getBaseURL()?>/admin" class="btn deep-purple accent-3 left"><i class="material-icons left">keyboard_arrow_left</i> Voltar</a>
  <a href="<?=getBaseURL()?>/bateria" class="btn deep-purple accent-3 right"><i class="material-icons left">add</i> Cadastrar</a>
</div>
<div class="container">
  <div class="row">
    <div class="page-header">
      <h1>Listagem de Baterias</h1>
    </div>
  </div>
  <?php if(isset($errors) && count($errors) > 0): ?>
  <div class="card red darken-1">
    <div class="row">
      <div class="card-content white-text">
        <?php foreach($errors as $chave => $error): ?>
          <p>
            <?=$chave.'. '.$error;?>
          </p>
        <?php endforeach; ?>
      </div>
    </div>
   </div>
  <?php endif; ?>
  <div class="row">
    <table class="striped centered responsive-table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Tipo</th>
          <th>Capacidade</th>
          <th>Carregamento Sem Fio</th>
          <th>Ações</th>
        </tr>
      </thead>
      <tbody>
        <?php
          foreach ($infos as $info){
            echo 
            "<tr>
              <td>".$info->id."</td>
              <td>".$info->tipo."</td>
              <td>".$info->capacidade."</td>
              <td>".(($info->carregamento_semfio) == 1 ? 'Sim' : 'Não')."</td>
              <td>
                <a value='{$info->id}' class='funcao-editar btn-floating waves-effect waves-light deep-purple accent-3'><i class='material-icons'>create</i></a>
                <a value='{$info->id}' class='funcao-excluir btn-floating waves-effect waves-light red darken-1'><i class='material-icons'>delete_forever</i></a>
              </td>
            </tr>";
          }
        ?>
      </tbody>
    </table>
  </div>
</div>


<script>
$(".funcao-excluir").click(function(){
  var id = $(this).attr("value");
  $.post("<?=getBaseURL()?>/excluir-bateria", { id: id })
  .done(function(data){
    console.log(data)
    window.location.href = "<?=getBaseURL()?>/visualizar-bateria"
  })
  .fail(function(err, errDtl, errThrown){
    console.log(err, errDtl, errThrown);
  })
})
  
$(".funcao-editar").click(function(){
  var id = $(this).attr("value");
  $.post("<?=getBaseURL()?>/editar-hardware", { id: id })
  .done(function(data){
    var dados = JSON.parse(data)
    $(".modal").modal('open')
    $("#id_campo").val(dados[0]['id'])
  })
  .fail(function(err, errDtl, errThrown){
    console.log(err, errDtl, errThrown);
  })
})
</script>
<!-- Modal Structure -->
<div id="modal1" class="modal bottom-sheet">
  <div class="modal-content">
    <?php require_once('battery.edit.php'); ?>
  </div>
</div>