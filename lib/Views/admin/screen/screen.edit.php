<div class="container">
  <div class="row">
    <div class="page-header">
      <h1>Editar tela</h1>
    </div>
  </div>
  <?php if(isset($errors) && count($errors) > 0): ?>
  <div class="card red darken-1">
    <div class="row">
      <div class="card-content white-text">
        <?php foreach($errors as $chave => $error): ?>
          <p>
            <?=($chave+1).'. '.$error;?>
          </p>
        <?php endforeach; ?>
      </div>
    </div>
   </div>
  <?php endif; ?>
  <div class="row">
    <form action="<?=getBaseURL()?>/atualizar-tela" method="POST">
      <div class="input-field">
        <input type="text" id="id_campo" name="id_campo" readonly="readonly">
      </div>
      <div class="input-field">
        <select name="tipo" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="IPS LCD">IPS LCD</option>
          <option value="AMOLED">AMOLED</option>
          <option value="Super AMOLED">Super AMOLED</option>
        </select>
        <label>Tipo de tela</label>
      </div>
      <div class="input-field">
        <select name="resolucao" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="HD (720 x 1520 pixels)">HD (720 x 1520 pixels)</option>
          <option value="Full HD+ (1080 x 2246 pixels)">Full HD+ (1080 x 2246 pixels)</option>
          <option value="2K (1600 x 2560 pixels)">2K (1600 x 2560 pixels)</option>
        </select>
        <label>Resolução</label>
      </div>
      <div class="field">
        <input type="number" name="densidade_pixel" id="densidade_pixel" required>
        <label>Densidade de Pixels</label>
      </div>
      <div class="field">
        <button class="btn deep-purple accent-3 right">Salvar</button>
      </div>
    </form>
  </div>
</div>