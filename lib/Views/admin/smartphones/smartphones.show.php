<div class="row" style="margin:10px 25px">
  <a href="<?=getBaseURL()?>/admin" class="btn deep-purple accent-3 left"><i class="material-icons left">keyboard_arrow_left</i> Voltar</a>
  <a href="<?=getBaseURL()?>/smartphones" class="btn deep-purple accent-3 right"><i class="material-icons left">add</i> Cadastrar</a>
</div>
<div class="container">
  <div class="row">
    <div class="page-header">
      <h1>Listagem de Smartphones</h1>
    </div>
  </div>
  <?php if(isset($errors) && count($errors) > 0): ?>
  <div class="card red darken-1">
    <div class="row">
      <div class="card-content white-text">
        <?php foreach($errors as $chave => $error): ?>
          <p>
            <?=$chave.'. '.$error;?>
          </p>
        <?php endforeach; ?>
      </div>
    </div>
   </div>
  <?php endif; ?>
  <div class="row">
    <table class="striped centered responsive-table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Marca</th>
          <th>Modelo</th>
        </tr>
      </thead>
      <tbody>
        <?php
          foreach($infos as $info){
            echo 
            "<tr>
              <td>".$info->id."</td>
              <td>".$info->marca."</td>
              <td>".$info->modelo."</td>
              <td>
                <a value='{$info->id}' class='funcao-editar btn-floating waves-effect waves-light deep-purple accent-3'><i class='material-icons'>create</i></a>
                <a value='{$info->id}' class='funcao-excluir btn-floating waves-effect waves-light red darken-1'><i class='material-icons'>delete_forever</i></a>
              </td>
            </tr>";
          }
        ?>
      </tbody>
    </table>
  </div>
</div>


<script>
$(".funcao-excluir").click(function(){
  var id = $(this).attr("value");
  $.post("<?=getBaseURL()?>/excluir-smartphones", { id: id })
  .done(function(data){
    console.log(data)
    window.location.href = "<?=getBaseURL()?>/visualizar-smartphones"
  })
  .fail(function(err, errDtl, errThrown){
    console.log(err, errDtl, errThrown);
  })
})
  
$(".funcao-editar").click(function(){
  var id = $(this).attr("value");
  $.post("<?=getBaseURL()?>/editar-smartphones", { id: id })
  .done(function(data){
    var dados = JSON.parse(data)
    console.log(dados)
    $(".modal").modal('open')
    $("#id_campo").val(dados[0]['id'])
    $("#modelo").val(dados[0]['modelo'])
  })
  .fail(function(err, errDtl, errThrown){
    console.log(err, errDtl, errThrown);
  })
})
</script>
<!-- Modal Structure -->
<div id="modal1" class="modal bottom-sheet">
  <div class="modal-content">
    <?php require_once('smartphones.edit.php'); ?>
  </div>
</div>