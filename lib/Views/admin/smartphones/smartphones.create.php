<div class="row" style="margin:10px 25px">
  <a href="<?=getBaseURL()?>/visualizar-smartphones" class="btn deep-purple accent-3 left"><i class="material-icons left">keyboard_arrow_left</i> Voltar</a>
  <!-- <a href="<?=getBaseURL()?>/hardware" class="btn deep-purple accent-3 right"><i class="material-icons left">add</i> Cadastrar</a> -->
</div>
<div class="container">
  <div class="row">
    <div class="page-header">
      <h1>Smartphones</h1>
    </div>
  </div>
  <?php if(isset($errors) && count($errors) > 0): ?>
  <div class="card red darken-1">
    <div class="row">
      <div class="card-content white-text">
        <?php foreach($errors as $chave => $error): ?>
          <p>
            <?=($chave+1).'. '.$error;?>
          </p>
        <?php endforeach; ?>
      </div>
    </div>
   </div>
  <?php endif; ?>
  <div class="row">
    <form action="<?=getBaseURL()?>/cadastrar-smartphones" method="POST">
      <div class="input-field col m6 s12">
        <select name="marca" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="Samsung">Samsung</option>
          <option value="Apple">Apple</option>
          <option value="Motorola">Motorola</option>
          <option value="Asus">Asus</option>
          <option value="LG">LG</option>
          <option value="Huawei">Huawei</option>
          <option value="Xiaomi">Xiaomi</option>
        </select>
        <label>Marca</label>
      </div>
      <div class="input-field col m6 s12">
        <input type="text" name="modelo">
        <label>Modelo</label>
      </div>
      <div class="input-field col m6 s12">
        <select name="bateria" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <?php
            foreach($infos['bateria'] as $bateria){
              echo "<option value='$bateria->id'>Tipo: $bateria->tipo | Capacidade: $bateria->capacidade | Carregamento sem fio: ".(($bateria->carregamento_semfio) == 1 ? 'Sim' : 'Não')."</option>";
            }
          ?>
        </select>
        <label>Bateria</label>
      </div>
      <div class="input-field col m6 s12">
        <select name="camera" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <?php
            foreach($infos['camera'] as $camera){
              echo "<option value='$camera->id'>Resolução Traseira: $camera->resolucao_traseira | Resolução Frontal: $camera->resolucao_frontal</option>";
            }
          ?>
        </select>
        <label>Câmera</label>
      </div>
      <div class="input-field col m6 s12">
        <select name="geral" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <?php
            foreach($infos['geral'] as $geral){
              echo "<option value='$geral->id'>SO: $geral->sistema | Versão SO: $geral->versao_sistema</option>";
            }
          ?>
        </select>
        <label>Geral</label>
      </div>
      <div class="input-field col m6 s12">
        <select name="hardware" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <?php
            foreach($infos['hardware'] as $hardware){
              echo "<option value='$hardware->id'>Núcleos CPU: $hardware->nucleos_cpu | Memória RAM: $hardware->resolucao_frontal | Armazenamento Interno: $hardware->armazenamento_interno | Cartão de Memória: ".(($hardware->acelerometro) == 1 ? 'Sim' : 'Não')."</option>";
            }
          ?>
        </select>
        <label>Hardware</label>
      </div>
      <div class="input-field col m6 s12">
        <select name="tela" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <?php
            foreach($infos['tela'] as $tela){
              echo "<option value='$tela->id'>Tipo: $tela->tipo | Resolução: $tela->resolucao | Densidade de Pixels: $tela->densidade_pixel</option>";
            }
          ?>
        </select>
        <label>Tela</label>
      </div>
      <div class="input-field col m6 s12">
        <select name="sensores" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <?php
            foreach($infos['sensores'] as $sensores){
              echo "<option value='$sensores->id_sensores'>Acelerômetro: ".(($sensores->acelerometro) == 1 ? 'Sim' : 'Não').
              " | Barômetro: ".(($sensores->barometro) == 1 ? 'Sim' : 'Não').
              " | Batimentos Cardíacos: ".(($sensores->batimentos_cardiacos) == 1 ? 'Sim' : 'Não').
              " | Bússola: ".(($sensores->bussola) == 1 ? 'Sim' : 'Não').
              " | Giroscópio: ".(($sensores->giroscopio) == 1 ? 'Sim' : 'Não').
              " | Impressão Digital: ".(($sensores->impressao_digital) == 1 ? 'Sim' : 'Não').
              " | Proximidade: ".(($sensores->proximidade) == 1 ? 'Sim' : 'Não').
              " | Temperatura: ".(($sensores->temperatura) == 1 ? 'Sim' : 'Não').
              " | Umidade: ".(($sensores->umidade) == 1 ? 'Sim' : 'Não').
              "</option>";
            }
          ?>
        </select>
        <label>Sensores</label>
      </div>
      
      <div class="field col m12">
        <button class="btn deep-purple accent-3 right">Cadastrar</button>
      </div>
    </form>
  </div>
</div>