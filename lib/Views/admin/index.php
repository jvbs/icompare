<div class="container">
  <div class="row">
    <div class="page-header">
      <h1>Dispositivos</h1>
    </div>
    <div class="col s12 m4">
      <div class="card">
        <div class="card-image">
          <img src="assets/img/celulares_sis.jpg">
          <a href="<?=getBaseURL().'/visualizar-smartphones'?>" class="btn-floating halfway-fab waves-effect waves-light deep-purple accent-3"><i class="material-icons">folder_open</i></a>
        </div>
        <div class="card-content">
          <span>Smartphones</span>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="page-header">
      <h1>Especificações</h1>
    </div>
    <div class="col s12 m4">
      <div class="card">
        <div class="card-image">
          <img src="assets/img/hardware_sis.jpg">
          <a href="<?=getBaseURL().'/visualizar-hardware'?>" class="btn-floating halfway-fab waves-effect waves-light deep-purple accent-3">
            <i class="material-icons">folder_open</i>
          </a>
        </div>
        <div class="card-content">
          <span>Hardware</span>
        </div>
      </div>
    </div>
    <div class="col s12 m4">
      <div class="card">
        <div class="card-image">
          <img src="assets/img/camera_sis.jpg">
          <a href="<?=getBaseURL().'/visualizar-camera'?>" class="btn-floating halfway-fab waves-effect waves-light deep-purple accent-3">
            <i class="material-icons">folder_open</i>
          </a>
        </div>
        <div class="card-content">
          <span>Câmera</span>
        </div>
      </div>
    </div>
    <div class="col s12 m4">
      <div class="card">
        <div class="card-image">
          <img src="assets/img/bateria_sis.jpg">
          <a href="<?=getBaseURL().'/visualizar-bateria'?>" class="btn-floating halfway-fab waves-effect waves-light deep-purple accent-3">
            <i class="material-icons">folder_open</i>
          </a>
        </div>
        <div class="card-content">
          <span>Bateria</span>
        </div>
      </div>
    </div>
    <div class="col s12 m4">
      <div class="card">
        <div class="card-image">
          <img src="assets/img/geral_sis.jpg">
          <a href="<?=getBaseURL().'/visualizar-geral'?>" class="btn-floating halfway-fab waves-effect waves-light deep-purple accent-3">
            <i class="material-icons">folder_open</i>
          </a>
        </div>
        <div class="card-content">
          <span>Geral</span>
        </div>
      </div>
    </div>
    <div class="col s12 m4">
      <div class="card">
        <div class="card-image">
          <img src="assets/img/tela_sis.jpg">
          <a href="<?=getBaseURL().'/visualizar-tela'?>" class="btn-floating halfway-fab waves-effect waves-light deep-purple accent-3">
            <i class="material-icons">folder_open</i>
          </a>
        </div>
        <div class="card-content">
          <span>Tela</span>
        </div>
      </div>
    </div>
    <div class="col s12 m4">
      <div class="card">
        <div class="card-image">
          <img src="assets/img/sensores_sis.jpg">
          <a href="<?=getBaseURL().'/visualizar-sensores'?>" class="btn-floating halfway-fab waves-effect waves-light deep-purple accent-3">
            <i class="material-icons">folder_open</i>
          </a>
        </div>
        <div class="card-content">
          <span>Sensores</span>
        </div>
      </div>
    </div>
  </div>
</div>