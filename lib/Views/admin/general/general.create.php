<div class="row" style="margin:10px 25px">
  <a href="<?=getBaseURL()?>/visualizar-geral" class="btn deep-purple accent-3 left"><i class="material-icons left">keyboard_arrow_left</i> Voltar</a>
  <!-- <a href="<?=getBaseURL()?>/tela" class="btn deep-purple accent-3 right"><i class="material-icons left">add</i> Cadastrar</a> -->
</div>
<div class="container">
  <div class="row">
    <div class="page-header">
      <h1>Geral</h1>
    </div>
  </div>
  <?php if(isset($errors) && count($errors) > 0): ?>
  <div class="card red darken-1">
    <div class="row">
      <div class="card-content white-text">
        <?php foreach($errors as $chave => $error): ?>
          <p>
            <?=($chave+1).'. '.$error;?>
          </p>
        <?php endforeach; ?>
      </div>
    </div>
   </div>
  <?php endif; ?>
  <div class="row">
    <form action="<?=getBaseURL()?>/cadastrar-geral" method="POST">
      <div class="input-field">
        <select name="sistema" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="Android">Android</option>
          <option value="iOS">iOS</option>
        </select>
        <label>Sistema Operacional</label>
      </div>
      <div class="input-field">
        <select name="versao_sistema" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="Android 6.0 - Marshmallow">Android 6.0 - Marshmallow</option>
          <option value="Android 7.0/7.1 - Nougat">Android 7.0/7.1 - Nougat</option>
          <option value="Android 8.0/8.1 - Oreo">Android 8.0/8.1 - Oreo</option>
          <option value="Android 9.0 - Pie">Android 9.0 - Pie</option>
          <option value="iOS 10">iOS 10</option>
          <option value="iOS 11">iOS 11</option>
          <option value="iOS 12">iOS 12</option>
          <option value="iOS 13">iOS 11</option>
        </select>
        <label>Versão do Sistema Operacional</label>
      </div>
      <div class="field">
        <button class="btn deep-purple accent-3 right">Cadastrar</button>
      </div>
    </form>
  </div>
</div>