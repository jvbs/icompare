<div class="row" style="margin:10px 25px">
  <a href="<?=getBaseURL()?>/visualizar-camera" class="btn deep-purple accent-3 left"><i class="material-icons left">keyboard_arrow_left</i> Voltar</a>
  <!-- <a href="<?=getBaseURL()?>/camera" class="btn deep-purple accent-3 right"><i class="material-icons left">add</i> Cadastrar</a> -->
</div>
<div class="container">
  <div class="row">
    <div class="page-header">
      <h1>Câmera</h1>
    </div>
  </div>
  <?php if(isset($errors) && count($errors) > 0): ?>
  <div class="card red darken-1">
    <div class="row">
      <div class="card-content white-text">
        <?php foreach($errors as $chave => $error): ?>
          <p>
            <?=($chave+1).'. '.$error;?>
          </p>
        <?php endforeach; ?>
      </div>
    </div>
   </div>
  <?php endif; ?>
  <div class="row">
    <form action="<?=getBaseURL()?>/cadastrar-camera" method="POST">
      <div class="input-field">
        <input type="number" name="resolucao_traseira" required>
        <label>Resolução da câmera traseira (em MP)</label>
      </div>
      <div class="input-field">
        <input type="number" name="resolucao_frontal" required>
        <label>Resolução da câmera frontal (em MP)</label>
      </div>
      <div class="field">
        <button class="btn deep-purple accent-3 right">Cadastrar</button>
      </div>
    </form>
  </div>
</div>