<div class="container">
  <div class="row">
    <div class="page-header">
      <h1>Editar câmera</h1>
    </div>
  </div>
  <?php if(isset($errors) && count($errors) > 0): ?>
  <div class="card red darken-1">
    <div class="row">
      <div class="card-content white-text">
        <?php foreach($errors as $chave => $error): ?>
          <p>
            <?=($chave+1).'. '.$error;?>
          </p>
        <?php endforeach; ?>
      </div>
    </div>
   </div>
  <?php endif; ?>
  <div class="row">
    <form action="<?=getBaseURL()?>/atualizar-camera" method="POST">
      <div class="field">
        <input type="text" id="id_campo" name="id_campo" readonly="readonly">
        <label>ID</label>
      </div>
      <div class="field">
        <input type="number" name="resolucao_traseira" id="resolucao_traseira" required>
        <label>Resolução da câmera traseira (em MP)</label>
      </div>
      <div class="field">
        <input type="number" name="resolucao_frontal" id="resolucao_frontal" required>
        <label>Resolução da câmera frontal (em MP)</label>
      </div>
      <div class="field">
        <button class="btn deep-purple accent-3 right">Cadastrar</button>
      </div>
    </form>
  </div>
</div>