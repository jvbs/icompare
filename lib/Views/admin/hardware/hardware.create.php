<div class="row" style="margin:10px 25px">
  <a href="<?=getBaseURL()?>/visualizar-hardware" class="btn deep-purple accent-3 left"><i class="material-icons left">keyboard_arrow_left</i> Voltar</a>
  <!-- <a href="<?=getBaseURL()?>/hardware" class="btn deep-purple accent-3 right"><i class="material-icons left">add</i> Cadastrar</a> -->
</div>

<div class="container">
  <div class="row">
    <div class="page-header">
      <h1>Hardware</h1>
    </div>
  </div>
  <?php if(isset($errors) && count($errors) > 0): ?>
  <div class="card red darken-1">
    <div class="row">
      <div class="card-content white-text">
        <?php foreach($errors as $chave => $error): ?>
          <p>
            <?=($chave+1).'. '.$error;?>
          </p>
        <?php endforeach; ?>
      </div>
    </div>
   </div>
  <?php endif; ?>
  <div class="row">
    <form action="<?=getBaseURL()?>/cadastrar-hardware" method="POST">
      <div class="input-field">
        <select name="nucleos_cpu" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="Dual-Core">Dual-Core</option>
          <option value="Quad-Core">Quad-Core</option>
          <option value="Octa-Core">Octa-Core</option>
        </select>
        <label>Núcleos CPU</label>
      </div>
      <div class="input-field">
        <select name="memoria_ram" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="1GB">1GB</option>
          <option value="2GB">2GB</option>
          <option value="3GB">3GB</option>
          <option value="4GB">4GB</option>
          <option value="6GB">6GB</option>
          <option value="8GB">8GB</option>
          <option value="12GB">12GB</option>
        </select>
        <label>Memória RAM</label>
      </div>
      <div class="input-field">
        <select name="armazenamento_interno" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="8GB">8GB</option>
          <option value="16GB">16GB</option>
          <option value="32GB">32GB</option>
          <option value="64GB">64GB</option>
          <option value="128GB">128GB</option>
          <option value="256GB">256GB</option>
        </select>
        <label>Armazenamento Interno</label>
      </div>
      <div class="input-field">
        <select name="cartao_memoria" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="Sim">Sim</option>
          <option value="Não">Não</option>
        </select>
        <label>Suporte Cartão SD</label>
      </div>
      <div class="field">
        <button class="btn deep-purple accent-3 right">Cadastrar</button>
      </div>
    </form>
  </div>
</div>