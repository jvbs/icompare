<div class="row" style="margin:10px 25px">
  <a href="<?=getBaseURL()?>/admin" class="btn deep-purple accent-3 left"><i class="material-icons left">keyboard_arrow_left</i> Voltar</a>
  <a href="<?=getBaseURL()?>/sensores" class="btn deep-purple accent-3 right"><i class="material-icons left">add</i> Cadastrar</a>
</div>
<div class="container">
  <div class="row">
    <div class="page-header">
      <h1>Listagem de Sensores</h1>
    </div>
  </div>
  <?php if(isset($errors) && count($errors) > 0): ?>
  <div class="card red darken-1">
    <div class="row">
      <div class="card-content white-text">
        <?php foreach($errors as $chave => $error): ?>
          <p>
            <?=$chave.'. '.$error;?>
          </p>
        <?php endforeach; ?>
      </div>
    </div>
   </div>
  <?php endif; ?>
  <div class="row">
    <table class="striped centered responsive-table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Acelerômetro</th>
          <th>Barômetro</th>
          <th>Batimentos Cardíacos</th>
          <th>Bússola</th>
          <th>Giroscópio</th>
          <th>Impressão Digital</th>
          <th>Sensor de Proximidade</th>
          <th>Temperatura</th>
          <th>Umidade</th>
          <th>Ações</th>
        </tr>
      </thead>
      <tbody>
        <?php
          foreach ($infos as $info){
            echo 
            "<tr>
              <td>".$info->id_sensores."</td>
              <td>".(($info->acelerometro) == 1 ? 'Sim' : 'Não')."</td>
              <td>".(($info->barometro) == 1 ? 'Sim' : 'Não')."</td>
              <td>".(($info->batimentos_cardiacos) == 1 ? 'Sim' : 'Não')."</td>
              <td>".(($info->bussola) == 1 ? 'Sim' : 'Não')."</td>
              <td>".(($info->giroscopio) == 1 ? 'Sim' : 'Não')."</td>
              <td>".(($info->impressao_digital) == 1 ? 'Sim' : 'Não')."</td>
              <td>".(($info->proximidade) == 1 ? 'Sim' : 'Não')."</td>
              <td>".(($info->temperatura) == 1 ? 'Sim' : 'Não')."</td>
              <td>".(($info->umidade) == 1 ? 'Sim' : 'Não')."</td>
              <td>
                <a value='{$info->id_sensores}' class='funcao-editar btn-floating waves-effect waves-light deep-purple accent-3'><i class='material-icons'>create</i></a>
                <a value='{$info->id_sensores}' class='funcao-excluir btn-floating waves-effect waves-light red darken-1'><i class='material-icons'>delete_forever</i></a>
              </td>
            </tr>";
          }
        ?>
      </tbody>
    </table>
  </div>
</div>


<script>
$(".funcao-excluir").click(function(){
  var id = $(this).attr("value");
  $.post("<?=getBaseURL()?>/excluir-sensores", { id: id })
  .done(function(data){
    console.log(data)
    // window.location.href = "<?=getBaseURL()?>/visualizar-sensores"
  })
  .fail(function(err, errDtl, errThrown){
    console.log(err, errDtl, errThrown);
  })
})
  
$(".funcao-editar").click(function(){
  var id = $(this).attr("value");
  $.post("<?=getBaseURL()?>/editar-sensores", { id: id })
  .done(function(data){
    var dados = JSON.parse(data)
    console.log(dados)
    $(".modal").modal('open')
    $("#id_campo").val(dados[0]['id_sensores'])
  })
  .fail(function(err, errDtl, errThrown){
    console.log(err, errDtl, errThrown);
  })
})
</script>
<!-- Modal Structure -->
<div id="modal1" class="modal bottom-sheet">
  <div class="modal-content">
    <?php require_once('sensors.edit.php'); ?>
  </div>
</div>