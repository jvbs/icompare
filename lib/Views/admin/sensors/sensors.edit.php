<div class="container">
  <div class="row">
    <div class="page-header">
      <h1>Sensores</h1>
    </div>
  </div>
  <?php if(isset($errors) && count($errors) > 0): ?>
  <div class="card red darken-1">
    <div class="row">
      <div class="card-content white-text">
        <?php foreach($errors as $chave => $error): ?>
          <p>
            <?=($chave+1).'. '.$error;?>
          </p>
        <?php endforeach; ?>
      </div>
    </div>
   </div>
  <?php endif; ?>
  <div class="row">
    <form action="<?=getBaseURL()?>/atualizar-sensores" method="POST">
      <div class="input-field">
        <input type="text" id="id_campo" name="id_campo" readonly="readonly">
      </div>
      <div class="input-field col m6 s12">
        <select name="acelerometro" required>
            <option value="" disabled selected>Selecione uma opção</option>
            <option value="Sim">Sim</option>
            <option value="Não">Não</option>
          </select>
          <label>Acelerômetro</label>
      </div>
      <div class="input-field col m6 s12">
        <select name="barometro" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="Sim">Sim</option>
          <option value="Não">Não</option>
        </select>
        <label>Barômetro</label>
      </div>
      <div class="input-field col m6 s12">
        <select name="batimentos_cardiacos" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="Sim">Sim</option>
          <option value="Não">Não</option>
        </select>
        <label>Batimentos Cardíacos</label>
      </div>
      <div class="input-field col m6 s12">
        <select name="bussola" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="Sim">Sim</option>
          <option value="Não">Não</option>
        </select>
        <label>Bússola</label>
      </div>
      <div class="input-field col m6 s12">
        <select name="giroscopio" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="Sim">Sim</option>
          <option value="Não">Não</option>
        </select>
        <label>Giroscópio</label>
      </div>
      <div class="input-field col m6 s12">
        <select name="impressao_digital" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="Sim">Sim</option>
          <option value="Não">Não</option>
        </select>
        <label>Impressão Digital</label>
      </div>
      <div class="input-field col m6 s12">
        <select name="proximidade" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="Sim">Sim</option>
          <option value="Não">Não</option>
        </select>
        <label>Sensor de Proximidade</label>
      </div>
      <div class="input-field col m6 s12">
        <select name="temperatura" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="Sim">Sim</option>
          <option value="Não">Não</option>
        </select>
        <label>Temperatura</label>
      </div>
      <div class="input-field col m6 s12">
        <select name="umidade" required>
          <option value="" disabled selected>Selecione uma opção</option>
          <option value="Sim">Sim</option>
          <option value="Não">Não</option>
        </select>
        <label>Umidade</label>
      </div>
      <div class="field col m12">
        <button class="btn deep-purple accent-3 right">Salvar</button>
      </div>
    </form>
  </div>
</div>