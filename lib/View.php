<?php
/**
 * Script de carregamento das views
 */

class View {
  public static function make($view, array $prmns = array()){
    // criando as variaveis do array $prmns
    extract($prmns);
    // incluindo a view
    require_once viewsPath().'templates/template.php';
   }
}