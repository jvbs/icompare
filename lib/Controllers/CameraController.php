<?php
namespace Controllers;

class CameraController {
  public static function create(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();
    // redireciona usuario
    \View::make('admin/camera/camera.create');
  }

  public static function show(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    $user = \Auth::user();
    // buscando dados
    $infos = \Models\Camera::all();

    \View::make('admin/camera/camera.show', compact('user', 'infos'));
  }

  public static function edit(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    $id = isset($_POST['id']) ? $_POST['id'] : null;
    $DB = new \DB;
    $sql = "SELECT * from camera where id = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
    if($stmt->execute()){
      $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

      echo json_encode($rows);
    } else {
      $errors[] = 'Ocorreu um erro na edição. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/camera/camera.show', compact('errors'));
    }
  }

  public static function delete(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();
    
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
      $status = 0;
      $id = isset($_POST['id']) ? $_POST['id'] : null;
      // criando cadastro
      $DB = new \DB;
      $sql = "UPDATE camera set status = :status where id = :id";
      $stmt = $DB->prepare($sql);
      $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
      $stmt->bindParam(':status', $status, \PDO::PARAM_INT);

      if($stmt->execute()){
        echo 'Excluído com sucesso!';
        // redirecionando caso ocorra corretamente o cadastro
        // redirect(getBaseURL().'/visualizar-camera');
      } else {
        $errors[] = 'Ocorreu um erro na exclusão. Tente novamente mais tarde';
        // redirecionando para pagina com erro
        return \View::make('admin/camera/camera.show', compact('errors'));
      }
    }
  }

  public static function update(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    // validacao dados recebidos post
    $id_campo = isset($_POST['id_campo']) ? $_POST['id_campo'] : null;
    $resolucao_traseira = isset($_POST['resolucao_traseira']) ? $_POST['resolucao_traseira']." MP" : null;
    $resolucao_frontal = isset($_POST['resolucao_frontal']) ? $_POST['resolucao_frontal']." MP" : null;
    $status = 1;

    // gerando erros
    $errors = [];
    if(empty($resolucao_traseira)){
      $errors[] = 'Informe a resolução da câmera traseira.';
    }
    if(empty($resolucao_frontal)){
      $errors[] = 'Informe a resolução da câmera frontal.';
    }

    if(count($errors) > 0){
      return \View::make('admin/camera/camera.create', compact('errors'));
    }

    // criando cadastro
    $DB = new \DB;
    $sql = "UPDATE camera SET resolucao_traseira = :resolucao_traseira, resolucao_frontal = :resolucao_frontal WHERE id = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':resolucao_traseira', $resolucao_traseira);
    $stmt->bindParam(':resolucao_frontal', $resolucao_frontal);
    $stmt->bindParam(':id', $id_campo, \PDO::PARAM_INT);

    if($stmt->execute()){
      // redirecionando caso ocorra corretamente o cadastro
      redirect(getBaseURL().'/visualizar-camera');
    } else {
      $errors[] = 'Ocorreu um erro no cadastro. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/camera/camera.create', compact('errors'));
    }
    
  }

  public static function store(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    // validacao dados recebidos post
    $resolucao_traseira = isset($_POST['resolucao_traseira']) ? $_POST['resolucao_traseira']." MP" : null;
    $resolucao_frontal = isset($_POST['resolucao_frontal']) ? $_POST['resolucao_frontal']." MP" : null;
    $status = 1;

    // gerando erros
    $errors = [];
    if(empty($resolucao_traseira)){
      $errors[] = 'Informe a resolução da câmera traseira.';
    }
    if(empty($resolucao_frontal)){
      $errors[] = 'Informe a resolução da câmera frontal.';
    }

    if(count($errors) > 0){
      return \View::make('admin/camera/camera.create', compact('errors'));
    }

    // criando cadastro
    $DB = new \DB;
    $sql = "INSERT into camera values (null, :resolucao_traseira, :resolucao_frontal, :status)";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':resolucao_traseira', $resolucao_traseira);
    $stmt->bindParam(':resolucao_frontal', $resolucao_frontal);
    $stmt->bindParam(':status', $status, \PDO::PARAM_INT);

    if($stmt->execute()){
      // redirecionando caso ocorra corretamente o cadastro
      redirect(getBaseURL().'/visualizar-camera');
    } else {
      $errors[] = 'Ocorreu um erro no cadastro. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/camera/camera.create', compact('errors'));
    }
    
  }
}