<?php
namespace Controllers;

class CompareController {
  public static function compare(){
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
      $celular_1 = isset($_POST['celular_1']) ? $_POST['celular_1'] : null;
      $celular_2 = isset($_POST['celular_2']) ? $_POST['celular_2'] : null;
      // iniciando consulta
      $DB = new \DB;
      $sql = "SELECT a.marca,
       a.modelo,
       b.sistema,
       b.versao_sistema,
       c.nucleos_cpu,
       c.memoria_ram,
       c.armazenamento_interno,
       c.cartao_memoria,
       d.resolucao_traseira,
       d.resolucao_frontal,
       e.tipo as tipo_bateria,
       e.capacidade,
       e.carregamento_semfio,
       f.tipo  as tipo_tela,
       f.resolucao,
       f.densidade_pixel,
       g.acelerometro,
       g.barometro,
       g.batimentos_cardiacos,
       g.bussola,
       g.giroscopio,
       g.impressao_digital,
       g.proximidade,
       g.temperatura,
       g.umidade 
       FROM `celulares` a 
       left join geral b on a.geral = b.id 
       left join hardware c on a.hardware = c.id 
       left join camera d on a.camera = d.id 
       left join bateria e on a.bateria = e.id 
       left join tela f on a.tela = f.id 
       left join sensores g on a.sensores = g.id_sensores 
       WHERE a.id = :id";
      // primeiro celular
      $stmt_cel1 = $DB->prepare($sql);
      $stmt_cel1->bindParam(':id', $celular_1, \PDO::PARAM_INT);
      if($stmt_cel1->execute()){
        $rows1 = $stmt_cel1->fetchAll(\PDO::FETCH_OBJ);
      }
      // segundo celular
      $stmt_cel2 = $DB->prepare($sql);
      $stmt_cel2->bindParam(':id', $celular_2, \PDO::PARAM_INT);
      if($stmt_cel2->execute()){
        $rows2 = $stmt_cel2->fetchAll(\PDO::FETCH_OBJ);
      }

      $rows = [
        "celular_1" => $rows1,
        "celular_2" => $rows2
      ];

      echo json_encode($rows);
    }
  }
}