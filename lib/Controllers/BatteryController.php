<?php
namespace Controllers;

class BatteryController {
  public static function create(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();
    // redireciona usuario
    \View::make('admin/battery/battery.create');
  }

  public static function show(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    $user = \Auth::user();
    // buscando dados
    $infos = \Models\Battery::all();

    \View::make('admin/battery/battery.show', compact('user', 'infos'));
  }

  public static function edit(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    $id = isset($_POST['id']) ? $_POST['id'] : null;
    $DB = new \DB;
    $sql = "SELECT * from bateria where id = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
    if($stmt->execute()){
      $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

      echo json_encode($rows);
    } else {
      $errors[] = 'Ocorreu um erro na edição. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/battery/battery.show', compact('errors'));
    }
  }

  public static function delete(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();
    
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
      $status = 0;
      $id = isset($_POST['id']) ? $_POST['id'] : null;
      // criando cadastro
      $DB = new \DB;
      $sql = "UPDATE bateria set status = :status where id = :id";
      $stmt = $DB->prepare($sql);
      $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
      $stmt->bindParam(':status', $status, \PDO::PARAM_INT);

      if($stmt->execute()){
        echo 'Excluído com sucesso!';
        // redirecionando caso ocorra corretamente o cadastro
        // redirect(getBaseURL().'/visualizar-bateria');
      } else {
        $errors[] = 'Ocorreu um erro na exclusão. Tente novamente mais tarde';
        // redirecionando para pagina com erro
        return \View::make('admin/battery/battery.show', compact('errors'));
      }
    }
  }

  public static function update(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    // validacao dados recebidos post
    $id_campo = isset($_POST['id_campo']) ? $_POST['id_campo'] : null;
    $tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null;
    $capacidade = isset($_POST['capacidade']) ? $_POST['capacidade'] : null;
    $carregamento_semfio = isset($_POST['carregamento_semfio']) ? $_POST['carregamento_semfio'] : null;

    // gerando erros
    $errors = [];
    if(empty($tipo)){
      $errors[] = 'Informe o tipo da bateria.';
    }
    if(empty($capacidade)){
      $errors[] = 'Informe a capacidade da bateria.';
    }
    if(empty($carregamento_semfio)){
      $errors[] = 'Informe se há suporte ou não para carregamento sem fio.';
    }

    if(count($errors) > 0){
      return \View::make('admin/battery/battery.create', compact('errors'));
    }

    $status = 1;
    if($carregamento_semfio == 'Sim'){
      $carregamento_semfio = 1;
    } else {
      $carregamento_semfio = 0;
    }

    // criando cadastro
    $DB = new \DB;
    $sql = "UPDATE bateria SET tipo = :tipo, capacidade = :capacidade, carregamento_semfio = :carregamento_semfio WHERE id = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':tipo', $tipo);
    $stmt->bindParam(':capacidade', $capacidade, \PDO::PARAM_INT);
    $stmt->bindParam(':carregamento_semfio', $carregamento_semfio, \PDO::PARAM_INT);
    $stmt->bindParam(':id', $id_campo, \PDO::PARAM_INT);

    if($stmt->execute()){
      // redirecionando caso ocorra corretamente o cadastro
      redirect(getBaseURL().'/visualizar-bateria');
    } else {
      $errors[] = 'Ocorreu um erro no cadastro. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/battery/battery.create', compact('errors'));
    }
    
  }

  public static function store(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    // validacao dados recebidos post
    $tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null;
    $capacidade = isset($_POST['capacidade']) ? $_POST['capacidade'] : null;
    $carregamento_semfio = isset($_POST['carregamento_semfio']) ? $_POST['carregamento_semfio'] : null;

    // gerando erros
    $errors = [];
    if(empty($tipo)){
      $errors[] = 'Informe o tipo da bateria.';
    }
    if(empty($capacidade)){
      $errors[] = 'Informe a capacidade da bateria.';
    }
    if(empty($carregamento_semfio)){
      $errors[] = 'Informe se há suporte ou não para carregamento sem fio.';
    }

    if(count($errors) > 0){
      return \View::make('admin/battery/battery.create', compact('errors'));
    }

    $status = 1;
    if($carregamento_semfio == 'Sim'){
      $carregamento_semfio = 1;
    } else {
      $carregamento_semfio = 0;
    }

    // criando cadastro
    $DB = new \DB;
    $sql = "INSERT into bateria values (null, :tipo, :capacidade, :carregamento_semfio, :status)";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':tipo', $tipo);
    $stmt->bindParam(':capacidade', $capacidade, \PDO::PARAM_INT);
    $stmt->bindParam(':carregamento_semfio', $carregamento_semfio, \PDO::PARAM_INT);
    $stmt->bindParam(':status', $status, \PDO::PARAM_INT);

    if($stmt->execute()){
      // redirecionando caso ocorra corretamente o cadastro
      redirect(getBaseURL().'/visualizar-bateria');
    } else {
      $errors[] = 'Ocorreu um erro no cadastro. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/battery/battery.create', compact('errors'));
    }
    
  }
}