<?php
namespace Controllers;

class SmartphonesController {
  public static function create(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();
    // buscando dados
    $infos = [
      "bateria" => \Models\Battery::all(),
      "camera" => \Models\Camera::all(),
      "geral" => \Models\General::all(),
      "hardware" => \Models\Hardware::all(),
      "tela" => \Models\Screen::all(),
      "sensores" => \Models\Sensors::all(),
    ];
    // redireciona usuario
    \View::make('admin/smartphones/smartphones.create', compact('infos'));
  }

  public static function show(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    $user = \Auth::user();
    // buscando dados
    $infos = \Models\Smartphones::all();

    \View::make('admin/smartphones/smartphones.show', compact('user', 'infos'));
  }

  public static function edit(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    $id = isset($_POST['id']) ? $_POST['id'] : null;
    $DB = new \DB;
    $sql = "SELECT * from celulares where id = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
    if($stmt->execute()){
      $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

      echo json_encode($rows);
    } else {
      $errors[] = 'Ocorreu um erro na edição. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/smartphones/smartphones.show', compact('errors'));
    }
  }

  public static function delete(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();
    
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
      $status = 0;
      $id = isset($_POST['id']) ? $_POST['id'] : null;
      // criando cadastro
      $DB = new \DB;
      $sql = "UPDATE celulares set status = :status where id = :id";
      $stmt = $DB->prepare($sql);
      $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
      $stmt->bindParam(':status', $status, \PDO::PARAM_INT);

      if($stmt->execute()){
        echo 'Excluído com sucesso!';
        // redirecionando caso ocorra corretamente o cadastro
        // redirect(getBaseURL().'/visualizar-hardware');
      } else {
        $errors[] = 'Ocorreu um erro na exclusão. Tente novamente mais tarde';
        // redirecionando para pagina com erro
        return \View::make('admin/smartphones/smartphones.show', compact('errors'));
      }
    }
  }

  public static function update(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    // validacao dados recebidos post
    $id_campo = isset($_POST['id_campo']) ? $_POST['id_campo'] : null;
    $marca = isset($_POST['marca']) ? $_POST['marca'] : null;
    $modelo = isset($_POST['modelo']) ? $_POST['modelo'] : null;
    $bateria = isset($_POST['bateria']) ? $_POST['bateria'] : null;
    $camera = isset($_POST['camera']) ? $_POST['camera'] : null;
    $geral = isset($_POST['geral']) ? $_POST['geral'] : null;
    $hardware = isset($_POST['hardware']) ? $_POST['hardware'] : null;
    $tela = isset($_POST['tela']) ? $_POST['tela'] : null;
    $sensores = isset($_POST['sensores']) ? $_POST['sensores'] : null;
    $status = 1;

    // gerando erros
    $errors = [];
    if(empty($marca)){
      $errors[] = "Informe a marca.";
    }

    if(empty($modelo)){
      $errors[] = "Informe o modelo";
    }

    if(empty($bateria)){
      $errors[] = "Informe a bateria";
    }

    if(empty($camera)){
      $errors[] = "Informe a câmera";
    }

    if(empty($geral)){
      $errors[] = "Informe as informações gerais";
    }

    if(empty($hardware)){
      $errors[] = "Informe o hardware";
    }

    if(empty($tela)){
      $errors[] = "Informe a tela";
    }

    if(empty($sensores)){
      $errors[] = "Informe os sensores";
    }

    if(count($errors) > 0){
      return \View::make('admin/smartphones/smartphones.create', compact('errors'));
    }


    // criando cadastro
    $DB = new \DB;
    $sql = "UPDATE celulares SET marca = :marca, modelo = :modelo, bateria = :bateria, camera= :camera, geral = :geral, hardware = :hardware, tela = :tela, sensores= :sensores WHERE id = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':marca', $marca);
    $stmt->bindParam(':modelo', $modelo);
    $stmt->bindParam(':bateria', $bateria, \PDO::PARAM_INT);
    $stmt->bindParam(':camera', $camera, \PDO::PARAM_INT);
    $stmt->bindParam(':geral', $geral, \PDO::PARAM_INT);
    $stmt->bindParam(':hardware', $hardware, \PDO::PARAM_INT);
    $stmt->bindParam(':tela', $tela, \PDO::PARAM_INT);
    $stmt->bindParam(':sensores', $sensores, \PDO::PARAM_INT);
    $stmt->bindParam(':id', $id_campo, \PDO::PARAM_INT);

    if($stmt->execute()){
      // redirecionando caso ocorra corretamente o cadastro
      redirect(getBaseURL().'/visualizar-smartphones');
    } else {
      $errors[] = 'Ocorreu um erro no cadastro. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/smartphones/smartphones.create', compact('errors'));
    }
    
  }

  public static function store(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    // validacao dados recebidos post
    $marca = isset($_POST['marca']) ? $_POST['marca'] : null;
    $modelo = isset($_POST['modelo']) ? $_POST['modelo'] : null;
    $bateria = isset($_POST['bateria']) ? $_POST['bateria'] : null;
    $camera = isset($_POST['camera']) ? $_POST['camera'] : null;
    $geral = isset($_POST['geral']) ? $_POST['geral'] : null;
    $hardware = isset($_POST['hardware']) ? $_POST['hardware'] : null;
    $tela = isset($_POST['tela']) ? $_POST['tela'] : null;
    $sensores = isset($_POST['sensores']) ? $_POST['sensores'] : null;
    $status = 1;

    // gerando erros
    $errors = [];
    if(empty($marca)){
      $errors[] = "Informe a marca.";
    }

    if(empty($modelo)){
      $errors[] = "Informe o modelo";
    }

    if(empty($bateria)){
      $errors[] = "Informe a bateria";
    }

    if(empty($camera)){
      $errors[] = "Informe a câmera";
    }

    if(empty($geral)){
      $errors[] = "Informe as informações gerais";
    }

    if(empty($hardware)){
      $errors[] = "Informe o hardware";
    }

    if(empty($tela)){
      $errors[] = "Informe a tela";
    }

    if(empty($sensores)){
      $errors[] = "Informe os sensores";
    }

    if(count($errors) > 0){
      return \View::make('admin/smartphones/smartphones.create', compact('errors'));
    }

    // criando cadastro
    $DB = new \DB;
    $sql = "INSERT into celulares values (null, :marca, :modelo, :geral, :hardware, :camera, :bateria, :tela, :sensores, :status)";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':marca', $marca);
    $stmt->bindParam(':modelo', $modelo);
    $stmt->bindParam(':geral', $geral, \PDO::PARAM_INT);
    $stmt->bindParam(':hardware', $hardware, \PDO::PARAM_INT);
    $stmt->bindParam(':camera', $camera, \PDO::PARAM_INT);
    $stmt->bindParam(':bateria', $bateria, \PDO::PARAM_INT);
    $stmt->bindParam(':tela', $tela, \PDO::PARAM_INT);
    $stmt->bindParam(':sensores', $sensores, \PDO::PARAM_INT);
    $stmt->bindParam(':status', $status, \PDO::PARAM_INT);

    if($stmt->execute()){
      // redirecionando caso ocorra corretamente o cadastro
      redirect(getBaseURL().'/visualizar-smartphones');
    } else {
      $errors[] = 'Ocorreu um erro no cadastro. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/hardware/sensors.create', compact('errors'));
    }
    
  }
}