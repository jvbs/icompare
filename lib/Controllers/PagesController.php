<?php
/**
 * Controller de gerenciamento das páginas
 */
namespace Controllers;

class PagesController {
  public static function home(){
    $celulares = \Models\Smartphones::all();
    
    \View::make('home', compact('celulares'));
  }

  public static function admin(){
    \View::make('admin/index');
  }
}