<?php
namespace Controllers;

class ScreenController {
  public static function create(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();
    // redireciona usuario
    \View::make('admin/screen/screen.create');
  }

  public static function show(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    $user = \Auth::user();
    // buscando dados
    $infos = \Models\Screen::all();

    \View::make('admin/screen/screen.show', compact('user', 'infos'));
  }

  public static function edit(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    $id = isset($_POST['id']) ? $_POST['id'] : null;
    $DB = new \DB;
    $sql = "SELECT * from tela where id = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
    if($stmt->execute()){
      $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

      echo json_encode($rows);
    } else {
      $errors[] = 'Ocorreu um erro na edição. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/screen/screen.show', compact('errors'));
    }
  }

  public static function delete(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();
    
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
      $status = 0;
      $id = isset($_POST['id']) ? $_POST['id'] : null;
      // criando cadastro
      $DB = new \DB;
      $sql = "UPDATE tela set status = :status where id = :id";
      $stmt = $DB->prepare($sql);
      $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
      $stmt->bindParam(':status', $status, \PDO::PARAM_INT);

      if($stmt->execute()){
        echo 'Excluído com sucesso!';
        // redirecionando caso ocorra corretamente o cadastro
        // redirect(getBaseURL().'/visualizar-tela');
      } else {
        $errors[] = 'Ocorreu um erro na exclusão. Tente novamente mais tarde';
        // redirecionando para pagina com erro
        return \View::make('admin/screen/screen.show', compact('errors'));
      }
    }
  }

  public static function update(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    // validacao dados recebidos post
    $id_campo = isset($_POST['id_campo']) ? $_POST['id_campo'] : null;
    $tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null;
    $resolucao = isset($_POST['resolucao']) ? $_POST['resolucao'] : null;
    $densidade_pixel = isset($_POST['densidade_pixel']) ? $_POST['densidade_pixel'] : null;
    $status = 1;

    // gerando erros
    $errors = [];
    if(empty($tipo)){
      $errors[] = 'Informe o tipo da tela.';
    }
    if(empty($resolucao)){
      $errors[] = 'Informe a resolução da tela.';
    }
    if(empty($densidade_pixel)){
      $errors[] = 'Informe a densidade de pixels da tela.';
    }

    if(count($errors) > 0){
      return \View::make('admin/screen/screen.create', compact('errors'));
    }

    // criando cadastro
    $DB = new \DB;
    $sql = "UPDATE tela SET tipo = :tipo, resolucao = :resolucao, densidade_pixel = :densidade_pixel WHERE id = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':tipo', $tipo);
    $stmt->bindParam(':resolucao', $resolucao);
    $stmt->bindParam(':densidade_pixel', $densidade_pixel, \PDO::PARAM_INT);
    $stmt->bindParam(':id', $id_campo, \PDO::PARAM_INT);

    if($stmt->execute()){
      // redirecionando caso ocorra corretamente o cadastro
      redirect(getBaseURL().'/visualizar-tela');
    } else {
      $errors[] = 'Ocorreu um erro no cadastro. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/screen/screen.create', compact('errors'));
    }
    
  }

  public static function store(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    // validacao dados recebidos post
    $tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null;
    $resolucao = isset($_POST['resolucao']) ? $_POST['resolucao'] : null;
    $densidade_pixel = isset($_POST['densidade_pixel']) ? $_POST['densidade_pixel'] : null;
    $status = 1;

    // gerando erros
    $errors = [];
    if(empty($tipo)){
      $errors[] = 'Informe o tipo da tela.';
    }
    if(empty($resolucao)){
      $errors[] = 'Informe a resolução da tela.';
    }
    if(empty($densidade_pixel)){
      $errors[] = 'Informe a densidade de pixels da tela.';
    }

    if(count($errors) > 0){
      return \View::make('admin/screen/screen.create', compact('errors'));
    }

    // criando cadastro
    $DB = new \DB;
    $sql = "INSERT into tela values (null, :tipo, :resolucao, :densidade_pixel, :status)";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':tipo', $tipo);
    $stmt->bindParam(':resolucao', $resolucao);
    $stmt->bindParam(':densidade_pixel', $densidade_pixel, \PDO::PARAM_INT);
    $stmt->bindParam(':status', $status, \PDO::PARAM_INT);

    if($stmt->execute()){
      // redirecionando caso ocorra corretamente o cadastro
      redirect(getBaseURL().'/visualizar-tela');
    } else {
      $errors[] = 'Ocorreu um erro no cadastro. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/screen/screen.create', compact('errors'));
    }
    
  }
}