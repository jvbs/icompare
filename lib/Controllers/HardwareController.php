<?php
namespace Controllers;

class HardwareController {
  public static function create(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();
    // redireciona usuario
    \View::make('admin/hardware/hardware.create');
  }

  public static function show(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    $user = \Auth::user();
    // buscando dados
    $infos = \Models\Hardware::all();

    \View::make('admin/hardware/hardware.show', compact('user', 'infos'));
  }

  public static function edit(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    $id = isset($_POST['id']) ? $_POST['id'] : null;
    $DB = new \DB;
    $sql = "SELECT * from hardware where id = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
    if($stmt->execute()){
      $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

      echo json_encode($rows);
    } else {
      $errors[] = 'Ocorreu um erro na edição. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/hardware/hardware.show', compact('errors'));
    }
  }

  public static function delete(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();
    
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
      $status = 0;
      $id = isset($_POST['id']) ? $_POST['id'] : null;
      // criando cadastro
      $DB = new \DB;
      $sql = "UPDATE hardware set status = :status where id = :id";
      $stmt = $DB->prepare($sql);
      $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
      $stmt->bindParam(':status', $status, \PDO::PARAM_INT);

      if($stmt->execute()){
        echo 'Excluído com sucesso!';
        // redirecionando caso ocorra corretamente o cadastro
        // redirect(getBaseURL().'/visualizar-hardware');
      } else {
        $errors[] = 'Ocorreu um erro na exclusão. Tente novamente mais tarde';
        // redirecionando para pagina com erro
        return \View::make('admin/hardware/hardware.show', compact('errors'));
      }
    }
  }

  public static function update(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    // validacao dados recebidos post
    $id_campo = isset($_POST['id_campo']) ? $_POST['id_campo'] : null;
    $nucleos_cpu = isset($_POST['nucleos_cpu']) ? $_POST['nucleos_cpu'] : null;
    $memoria_ram = isset($_POST['memoria_ram']) ? $_POST['memoria_ram'] : null;
    $armazenamento_interno = isset($_POST['armazenamento_interno']) ? $_POST['armazenamento_interno'] : null;
    $cartao_memoria = isset($_POST['cartao_memoria']) ? $_POST['cartao_memoria'] : null;

    // gerando erros
    $errors = [];
    if(empty($nucleos_cpu)){
      $errors[] = 'Informe quantos núcleos tem a CPU.';
    }
    if(empty($memoria_ram)){
      $errors[] = 'Informe a quantidade de memória RAM.';
    }
    if(empty($armazenamento_interno)){
      $errors[] = 'Informe a quantidade de armazenamento interno.';
    }
    if(empty($cartao_memoria)){
      $errors[] = 'Informe se há suporte ou não para cartão de memória.';
    }

    if(count($errors) > 0){
      return \View::make('admin/hardware/hardware.show', compact('errors'));
    }

    $user = \Auth::user();
    $user_id = $user->getId();
    $now = date('Y-m-d H:i:s');
    $status = 1;
    if($cartao_memoria == 'Sim'){
      $cartao_memoria = 1;
    } else {
      $cartao_memoria = 0;
    }

    // criando cadastro
    $DB = new \DB;
    $sql = "UPDATE hardware SET nucleos_cpu = :nucleos_cpu, memoria_ram = :memoria_ram, armazenamento_interno = :armazenamento_interno, cartao_memoria = :cartao_memoria WHERE id = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':nucleos_cpu', $nucleos_cpu);
    $stmt->bindParam(':memoria_ram', $memoria_ram);
    $stmt->bindParam(':armazenamento_interno', $armazenamento_interno);
    $stmt->bindParam(':cartao_memoria', $cartao_memoria, \PDO::PARAM_INT);
    $stmt->bindParam(':id', $id_campo, \PDO::PARAM_INT);

    if($stmt->execute()){
      // redirecionando caso ocorra corretamente o cadastro
      redirect(getBaseURL().'/visualizar-hardware');
    } else {
      $errors[] = 'Ocorreu um erro no cadastro. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/hardware/hardware.create', compact('errors'));
    }
    
  }

  public static function store(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    // validacao dados recebidos post
    $nucleos_cpu = isset($_POST['nucleos_cpu']) ? $_POST['nucleos_cpu'] : null;
    $memoria_ram = isset($_POST['memoria_ram']) ? $_POST['memoria_ram'] : null;
    $armazenamento_interno = isset($_POST['armazenamento_interno']) ? $_POST['armazenamento_interno'] : null;
    $cartao_memoria = isset($_POST['cartao_memoria']) ? $_POST['cartao_memoria'] : null;

    // gerando erros
    $errors = [];
    if(empty($nucleos_cpu)){
      $errors[] = 'Informe quantos núcleos tem a CPU.';
    }
    if(empty($memoria_ram)){
      $errors[] = 'Informe a quantidade de memória RAM.';
    }
    if(empty($armazenamento_interno)){
      $errors[] = 'Informe a quantidade de armazenamento interno.';
    }
    if(empty($cartao_memoria)){
      $errors[] = 'Informe se há suporte ou não para cartão de memória.';
    }

    if(count($errors) > 0){
      return \View::make('admin/hardware/hardware.create', compact('errors'));
    }

    $user = \Auth::user();
    $user_id = $user->getId();
    $now = date('Y-m-d H:i:s');
    $status = 1;
    if($cartao_memoria == 'Sim'){
      $cartao_memoria = 1;
    } else {
      $cartao_memoria = 0;
    }

    // criando cadastro
    $DB = new \DB;
    $sql = "INSERT into hardware values (null, :nucleos_cpu, :memoria_ram, :armazenamento_interno, :cartao_memoria, :user_id, :status)";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':nucleos_cpu', $nucleos_cpu);
    $stmt->bindParam(':memoria_ram', $memoria_ram);
    $stmt->bindParam(':armazenamento_interno', $armazenamento_interno);
    $stmt->bindParam(':cartao_memoria', $cartao_memoria, \PDO::PARAM_INT);
    $stmt->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
    $stmt->bindParam(':status', $status, \PDO::PARAM_INT);

    if($stmt->execute()){
      // redirecionando caso ocorra corretamente o cadastro
      redirect(getBaseURL().'/visualizar-hardware');
    } else {
      $errors[] = 'Ocorreu um erro no cadastro. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/hardware/hardware.create', compact('errors'));
    }
    
  }
}