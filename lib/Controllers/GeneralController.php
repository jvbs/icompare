<?php
namespace Controllers;

class GeneralController {
  public static function create(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();
    // redireciona usuario
    \View::make('admin/general/general.create');
  }

  public static function show(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    $user = \Auth::user();
    // buscando dados
    $infos = \Models\General::all();

    \View::make('admin/general/general.show', compact('user', 'infos'));
  }

  public static function edit(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    $id = isset($_POST['id']) ? $_POST['id'] : null;
    $DB = new \DB;
    $sql = "SELECT * from tela where id = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
    if($stmt->execute()){
      $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

      echo json_encode($rows);
    } else {
      $errors[] = 'Ocorreu um erro na edição. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/general/general.show', compact('errors'));
    }
  }

  public static function delete(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();
    
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
      $status = 0;
      $id = isset($_POST['id']) ? $_POST['id'] : null;
      // criando cadastro
      $DB = new \DB;
      $sql = "UPDATE geral set status = :status where id = :id";
      $stmt = $DB->prepare($sql);
      $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
      $stmt->bindParam(':status', $status, \PDO::PARAM_INT);

      if($stmt->execute()){
        echo 'Excluído com sucesso!';
        // redirecionando caso ocorra corretamente o cadastro
        // redirect(getBaseURL().'/visualizar-geral');
      } else {
        $errors[] = 'Ocorreu um erro na exclusão. Tente novamente mais tarde';
        // redirecionando para pagina com erro
        return \View::make('admin/general/general.show', compact('errors'));
      }
    }
  }

  public static function update(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    // validacao dados recebidos post
    $id_campo = isset($_POST['id_campo']) ? $_POST['id_campo'] : null;
    $sistema = isset($_POST['sistema']) ? $_POST['sistema'] : null;
    $versao_sistema = isset($_POST['versao_sistema']) ? $_POST['versao_sistema'] : null;
    $status = 1;

    // gerando erros
    $errors = [];
    if(empty($sistema)){
      $errors[] = 'Informe o sistema operacional.';
    }
    if(empty($versao_sistema)){
      $errors[] = 'Informe a versão do sistema operacional.';
    }

    if(count($errors) > 0){
      return \View::make('admin/general/general.create', compact('errors'));
    }

    // criando cadastro
    $DB = new \DB;
    $sql = "UPDATE geral SET sistema = :sistema, versao_sistema = :versao_sistema WHERE id = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':sistema', $sistema);
    $stmt->bindParam(':versao_sistema', $versao_sistema);
    $stmt->bindParam(':id', $id_campo, \PDO::PARAM_INT);

    if($stmt->execute()){
      // redirecionando caso ocorra corretamente o cadastro
      redirect(getBaseURL().'/visualizar-geral');
    } else {
      $errors[] = 'Ocorreu um erro no cadastro. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/general/general.create', compact('errors'));
    }
    
  }

  public static function store(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    // validacao dados recebidos post
    $sistema = isset($_POST['sistema']) ? $_POST['sistema'] : null;
    $versao_sistema = isset($_POST['versao_sistema']) ? $_POST['versao_sistema'] : null;
    $status = 1;

    // gerando erros
    $errors = [];
    if(empty($sistema)){
      $errors[] = 'Informe o sistema operacional.';
    }
    if(empty($versao_sistema)){
      $errors[] = 'Informe a versão do sistema operacional.';
    }

    if(count($errors) > 0){
      return \View::make('admin/general/general.create', compact('errors'));
    }

    // criando cadastro
    $DB = new \DB;
    $sql = "INSERT into geral values (null, :sistema, :versao_sistema, :status)";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':sistema', $sistema);
    $stmt->bindParam(':versao_sistema', $versao_sistema);
    $stmt->bindParam(':status', $status, \PDO::PARAM_INT);

    if($stmt->execute()){
      // redirecionando caso ocorra corretamente o cadastro
      redirect(getBaseURL().'/visualizar-geral');
    } else {
      $errors[] = 'Ocorreu um erro no cadastro. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/general/general.create', compact('errors'));
    }
    
  }
}