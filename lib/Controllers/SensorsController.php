<?php
namespace Controllers;

class SensorsController {
  public static function create(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();
    // redireciona usuario
    \View::make('admin/sensors/sensors.create');
  }

  public static function show(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    $user = \Auth::user();
    // buscando dados
    $infos = \Models\Sensors::all();

    \View::make('admin/sensors/sensors.show', compact('user', 'infos'));
  }

  public static function edit(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    $id = isset($_POST['id']) ? $_POST['id'] : null;
    $DB = new \DB;
    $sql = "SELECT * from sensores where id_sensores = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
    if($stmt->execute()){
      $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

      echo json_encode($rows);
    } else {
      $errors[] = 'Ocorreu um erro na edição. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/sensors/sensors.show', compact('errors'));
    }
  }

  public static function delete(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();
    
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
      $status = 0;
      $id = isset($_POST['id']) ? $_POST['id'] : null;
      // criando cadastro
      $DB = new \DB;
      $sql = "UPDATE sensores set status = :status where id_sensores = :id";
      $stmt = $DB->prepare($sql);
      $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
      $stmt->bindParam(':status', $status, \PDO::PARAM_INT);

      if($stmt->execute()){
        echo 'Excluído com sucesso!';
        // redirecionando caso ocorra corretamente o cadastro
        // redirect(getBaseURL().'/visualizar-hardware');
      } else {
        $errors[] = 'Ocorreu um erro na exclusão. Tente novamente mais tarde';
        // redirecionando para pagina com erro
        return \View::make('admin/sensors/sensors.show', compact('errors'));
      }
    }
  }

  public static function update(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    // validacao dados recebidos post
    $id_campo = isset($_POST['id_campo']) ? $_POST['id_campo'] : null;
    $acelerometro = isset($_POST['acelerometro']) ? $_POST['acelerometro'] : null;
    $barometro = isset($_POST['barometro']) ? $_POST['barometro'] : null;
    $batimentos_cardiacos = isset($_POST['batimentos_cardiacos']) ? $_POST['batimentos_cardiacos'] : null;
    $bussola = isset($_POST['bussola']) ? $_POST['bussola'] : null;
    $giroscopio = isset($_POST['giroscopio']) ? $_POST['giroscopio'] : null;
    $impressao_digital = isset($_POST['impressao_digital']) ? $_POST['impressao_digital'] : null;
    $proximidade = isset($_POST['proximidade']) ? $_POST['proximidade'] : null;
    $temperatura = isset($_POST['temperatura']) ? $_POST['temperatura'] : null;
    $umidade = isset($_POST['umidade']) ? $_POST['umidade'] : null;

    // gerando erros
    $errors = [];
    if(empty($acelerometro)){
      $errors[] = "Informe o acelerometro";
    }

    if(empty($barometro)){
      $errors[] = "Informe o barometro";
    }

    if(empty($batimentos_cardiacos)){
      $errors[] = "Informe o batimentos cardiacos";
    }

    if(empty($bussola)){
      $errors[] = "Informe o bussola";
    }

    if(empty($giroscopio)){
      $errors[] = "Informe o giroscopio";
    }

    if(empty($impressao_digital)){
      $errors[] = "Informe a impressao_digital";
    }

    if(empty($proximidade)){
      $errors[] = "Informe o proximidade";
    }

    if(empty($temperatura)){
      $errors[] = "Informe o temperatura";
    }

    if(empty($umidade)){
      $errors[] = "Informe o umidade";
    }

    if(count($errors) > 0){
      return \View::make('admin/sensors/sensors.create', compact('errors'));
    }

    if($acelerometro == 'Sim'){
      $acelerometro = 1;
    } else {
      $acelerometro = 0;
    }

    if($barometro == 'Sim'){
      $barometro = 1;
    } else {
      $barometro = 0;
    }

    if($batimentos_cardiacos == 'Sim'){
      $batimentos_cardiacos = 1;
    } else {
      $batimentos_cardiacos = 0;
    }

    if($bussola == 'Sim'){
      $bussola = 1;
    } else {
      $bussola = 0;
    }

    if($giroscopio == 'Sim'){
      $giroscopio = 1;
    } else {
      $giroscopio = 0;
    }

    if($impressao_digital == 'Sim'){
      $impressao_digital = 1;
    } else {
      $impressao_digital = 0;
    }

    if($proximidade == 'Sim'){
      $proximidade = 1;
    } else {
      $proximidade = 0;
    }

    if($temperatura == 'Sim'){
      $temperatura = 1;
    } else {
      $temperatura = 0;
    }

    if($umidade == 'Sim'){
      $umidade = 1;
    } else {
      $umidade = 0;
    }

    $status = 1;

    // criando cadastro
    $DB = new \DB;
    $sql = "UPDATE sensores SET acelerometro = :acelerometro, barometro = :barometro, batimentos_cardiacos = :batimentos_cardiacos, bussola= :bussola, giroscopio = :giroscopio, impressao_digital = :impressao_digital, proximidade = :proximidade, temperatura= :temperatura,  umidade = :umidade WHERE id_sensores = :id";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':acelerometro', $acelerometro, \PDO::PARAM_INT);
    $stmt->bindParam(':barometro', $barometro, \PDO::PARAM_INT);
    $stmt->bindParam(':batimentos_cardiacos', $batimentos_cardiacos, \PDO::PARAM_INT);
    $stmt->bindParam(':bussola', $bussola, \PDO::PARAM_INT);
    $stmt->bindParam(':giroscopio', $giroscopio, \PDO::PARAM_INT);
    $stmt->bindParam(':impressao_digital', $impressao_digital, \PDO::PARAM_INT);
    $stmt->bindParam(':proximidade', $proximidade, \PDO::PARAM_INT);
    $stmt->bindParam(':temperatura', $temperatura, \PDO::PARAM_INT);
    $stmt->bindParam(':umidade', $umidade, \PDO::PARAM_INT);
    $stmt->bindParam(':id', $id_campo, \PDO::PARAM_INT);

    if($stmt->execute()){
      // redirecionando caso ocorra corretamente o cadastro
      redirect(getBaseURL().'/visualizar-sensores');
    } else {
      $errors[] = 'Ocorreu um erro no cadastro. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/hardware/sensors.create', compact('errors'));
    }
    
  }

  public static function store(){
    // verificando se o usuario esta logado
    \Auth::denyNotLoggedInUsers();

    // validacao dados recebidos post
    $acelerometro = isset($_POST['acelerometro']) ? $_POST['acelerometro'] : null;
    $barometro = isset($_POST['barometro']) ? $_POST['barometro'] : null;
    $batimentos_cardiacos = isset($_POST['batimentos_cardiacos']) ? $_POST['batimentos_cardiacos'] : null;
    $bussola = isset($_POST['bussola']) ? $_POST['bussola'] : null;
    $giroscopio = isset($_POST['giroscopio']) ? $_POST['giroscopio'] : null;
    $impressao_digital = isset($_POST['impressao_digital']) ? $_POST['impressao_digital'] : null;
    $proximidade = isset($_POST['proximidade']) ? $_POST['proximidade'] : null;
    $temperatura = isset($_POST['temperatura']) ? $_POST['temperatura'] : null;
    $umidade = isset($_POST['umidade']) ? $_POST['umidade'] : null;

    // gerando erros
    $errors = [];
    if(empty($acelerometro)){
      $errors[] = "Informe o acelerometro";
    }

    if(empty($barometro)){
      $errors[] = "Informe o barometro";
    }

    if(empty($batimentos_cardiacos)){
      $errors[] = "Informe o ($";
    }

    if(empty($bussola)){
      $errors[] = "Informe o bussola";
    }

    if(empty($giroscopio)){
      $errors[] = "Informe o giroscopio";
    }

    if(empty($impressao_digital)){
      $errors[] = "Informe o ($";
    }

    if(empty($proximidade)){
      $errors[] = "Informe o proximidade";
    }

    if(empty($temperatura)){
      $errors[] = "Informe o temperatura";
    }

    if(empty($umidade)){
      $errors[] = "Informe o umidade";
    }

    if(count($errors) > 0){
      return \View::make('admin/sensors/sensors.create', compact('errors'));
    }

    if($acelerometro == 'Sim'){
      $acelerometro = 1;
    } else {
      $acelerometro = 0;
    }

    if($barometro == 'Sim'){
      $barometro = 1;
    } else {
      $barometro = 0;
    }

    if($batimentos_cardiacos == 'Sim'){
      $batimentos_cardiacos = 1;
    } else {
      $batimentos_cardiacos = 0;
    }

    if($bussola == 'Sim'){
      $bussola = 1;
    } else {
      $bussola = 0;
    }

    if($giroscopio == 'Sim'){
      $giroscopio = 1;
    } else {
      $giroscopio = 0;
    }

    if($impressao_digital == 'Sim'){
      $impressao_digital = 1;
    } else {
      $impressao_digital = 0;
    }

    if($proximidade == 'Sim'){
      $proximidade = 1;
    } else {
      $proximidade = 0;
    }

    if($temperatura == 'Sim'){
      $temperatura = 1;
    } else {
      $temperatura = 0;
    }

    if($umidade == 'Sim'){
      $umidade = 1;
    } else {
      $umidade = 0;
    }

    $status = 1;

    // criando cadastro
    $DB = new \DB;
    $sql = "INSERT into sensores values (null, :acelerometro, :barometro, :batimentos_cardiacos, :bussola, :giroscopio, :impressao_digital, :proximidade, :temperatura, :umidade, :status)";
    $stmt = $DB->prepare($sql);
    $stmt->bindParam(':acelerometro', $acelerometro, \PDO::PARAM_INT);
    $stmt->bindParam(':barometro', $barometro, \PDO::PARAM_INT);
    $stmt->bindParam(':batimentos_cardiacos', $batimentos_cardiacos, \PDO::PARAM_INT);
    $stmt->bindParam(':bussola', $bussola, \PDO::PARAM_INT);
    $stmt->bindParam(':giroscopio', $giroscopio, \PDO::PARAM_INT);
    $stmt->bindParam(':impressao_digital', $impressao_digital, \PDO::PARAM_INT);
    $stmt->bindParam(':proximidade', $proximidade, \PDO::PARAM_INT);
    $stmt->bindParam(':temperatura', $temperatura, \PDO::PARAM_INT);
    $stmt->bindParam(':umidade', $umidade, \PDO::PARAM_INT);
    $stmt->bindParam(':status', $status, \PDO::PARAM_INT);

    if($stmt->execute()){
      // redirecionando caso ocorra corretamente o cadastro
      redirect(getBaseURL().'/visualizar-sensores');
    } else {
      $errors[] = 'Ocorreu um erro no cadastro. Tente novamente mais tarde';
      // redirecionando para pagina com erro
      return \View::make('admin/hardware/sensors.create', compact('errors'));
    }
    
  }
}