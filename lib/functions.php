<?php
/**
 * Arquivo de funções para uso geral
 */


/**
 * Verifica se o ambiente atual é de desenvolvimento
 */
function isDevEnv(){
  return ENV == 'dev';
}

/**
 * Retorna o caminho para o diretório com as views
 */
function viewsPath(){
  return APP_ROOT_PATH.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR;
}

/**
 * Retorna o caminho para o diretório de logs
 */
function logsPath(){
  return APP_ROOT_PATH.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR;
}

/**
 * Retorna a URL base da aplicação
 */
function getBaseURL(){
  return sprintf(
    "%s://%s%s%s",
    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
    $_SERVER['SERVER_NAME'],
    $_SERVER['SERVER_PORT'] == 80 ? '' : ':'.$_SERVER['SERVER_PORT'],
    APP_PATH != null ? APP_PATH : ''
  );
}

/**
 * Retorna a URL atual
 */
function getCurrentURL(){
  return getBaseURL().$_SERVER['REQUEST_URI'];
}

/**
 * Função de redirecionamento HTTP
 */
function redirect($url){
  header('Location: '.$url);
  exit;
}