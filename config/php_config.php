<?php
/*
 * Configurações do PHP
 */

/*
 * Lista com os hostnames dos ambientes de desenvolvimento.
 */
$devHostnames = [
    'zorin-vm', //pc jv
    'DESKTOP-15VVT0P', //notebook jv
    'DESKTOP-UATM344' // desktop jv
];

// Fuso-Horário
date_default_timezone_set('America/Sao_Paulo');

// Habilita todos os níveis de exibição de erros.
error_reporting(E_ALL | E_STRICT);

// pega o hostname da máquina em que a aplicação está rodando
$hostname = gethostname();

if(in_array($hostname, $devHostnames)){
    // ambiente de desenvolvimento
    define('ENV', 'dev');
    require_once APP_ROOT_PATH.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'env'.DIRECTORY_SEPARATOR.'dev.php';
} else {
    // ambiente de produção
    define('ENV', 'prod');
    require_once APP_ROOT_PATH.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'env'.DIRECTORY_SEPARATOR.'prod.php';
}