<?php
require_once 'init.php';

// retira as barras do  início e do final da string
$uri = trim(str_replace(APP_PATH, '', $_SERVER["REQUEST_URI"]), '/');

// array com cada valor entre as barras na URL
$GETS = explode("/", $uri);

// tratando o primeiro indice da rota
$page = isset($GETS[0]) && !empty($GETS[0]) ? $GETS[0] : '';

switch($page){
  case '':
    \Controllers\PagesController::home();
    break;
  case 'login':
    \Controllers\SessionsController::login();
    break;
  case 'logout':
    \Controllers\SessionsController::logout();
    break;
  case 'admin':
    \Controllers\PagesController::admin();    
    break;
  
  // compare
  case 'comparar':
    \Controllers\CompareController::compare();
    break;
  // hardware 
  case 'hardware':
    \Controllers\HardwareController::create();
    break;
  case 'cadastrar-hardware':
    \Controllers\HardwareController::store();
    break;
  case 'visualizar-hardware':
    \Controllers\HardwareController::show();
    break;
  case 'excluir-hardware':
    \Controllers\HardwareController::delete();
    break;
  case 'editar-hardware':
    \Controllers\HardwareController::edit();
    break;
  case 'atualizar-hardware':
      \Controllers\HardwareController::update();
      break;

  // sensores 
  case 'sensores':
    \Controllers\SensorsController::create();
    break;
  case 'cadastrar-sensores':
    \Controllers\SensorsController::store();
    break;
  case 'visualizar-sensores':
    \Controllers\SensorsController::show();
    break;
  case 'excluir-sensores':
    \Controllers\SensorsController::delete();
    break;
  case 'editar-sensores':
    \Controllers\SensorsController::edit();
    break;
  case 'atualizar-sensores':
    \Controllers\SensorsController::update();
    break;

  // bateria 
  case 'bateria':
    \Controllers\BatteryController::create();
    break;
  case 'cadastrar-bateria':
    \Controllers\BatteryController::store();
    break;
  case 'visualizar-bateria':
    \Controllers\BatteryController::show();
    break;
  case 'excluir-bateria':
    \Controllers\BatteryController::delete();
    break;
  case 'editar-bateria':
    \Controllers\BatteryController::edit();
    break;
  case 'atualizar-bateria':
    \Controllers\BatteryController::update();
    break;

  // tela 
  case 'tela':
    \Controllers\ScreenController::create();
    break;
  case 'cadastrar-tela':
    \Controllers\ScreenController::store();
    break;
  case 'visualizar-tela':
    \Controllers\ScreenController::show();
    break;
  case 'excluir-tela':
    \Controllers\ScreenController::delete();
    break;
  case 'editar-tela':
    \Controllers\ScreenController::edit();
    break;
  case 'atualizar-tela':
    \Controllers\ScreenController::update();
    break;
  
  // geral
  case 'geral':
    \Controllers\GeneralController::create();
    break;
  case 'cadastrar-geral':
    \Controllers\GeneralController::store();
    break;
  case 'visualizar-geral':
    \Controllers\GeneralController::show();
    break;
  case 'excluir-geral':
    \Controllers\GeneralController::delete();
    break;
  case 'editar-geral':
    \Controllers\GeneralController::edit();
    break;
  case 'atualizar-geral':
    \Controllers\GeneralController::update();
    break;

  // camera
  case 'camera':
    \Controllers\CameraController::create();
    break;
  case 'cadastrar-camera':
    \Controllers\CameraController::store();
    break;
  case 'visualizar-camera':
    \Controllers\CameraController::show();
    break;
  case 'excluir-camera':
    \Controllers\CameraController::delete();
    break;
  case 'editar-camera':
    \Controllers\CameraController::edit();
    break;
  case 'atualizar-camera':
    \Controllers\CameraController::update();
    break;

  // camera
  case 'smartphones':
    \Controllers\SmartphonesController::create();
    break;
  case 'cadastrar-smartphones':
    \Controllers\SmartphonesController::store();
    break;
  case 'visualizar-smartphones':
    \Controllers\SmartphonesController::show();
    break;
  case 'excluir-smartphones':
    \Controllers\SmartphonesController::delete();
    break;
  case 'editar-smartphones':
    \Controllers\SmartphonesController::edit();
    break;
  case 'atualizar-smartphones':
    \Controllers\SmartphonesController::update();
    break;

  // 404  
  default:
    echo "Página não encontrada";
}