<?php
require_once "init.php";

$nome = 'Administrador iCompare';
$data_nascimento = '2019-09-01';
$email = 'admin@icompare.com.br';
$senha = Hash::hash_pass('admin');
$status = 1;
$admin = 1;
$data = date('Y-m-d H:i:s');

$sql = "INSERT INTO usuarios (nome, data_nascimento, email, senha, status, admin, criado_em, atualizado_em)
        VALUES (:nome, :data_nascimento, :email, :senha, :status, :admin, :criado_em, :atualizado_em)";

$DB = new DB;
$stmt = $DB->prepare($sql);


$stmt->bindParam(':nome', $nome);
$stmt->bindParam(':data_nascimento', $data_nascimento);
$stmt->bindParam(':email', $email);
$stmt->bindParam(':senha', $senha);
$stmt->bindParam(':status', $status);
$stmt->bindParam(':admin', $admin);
$stmt->bindParam(':criado_em', $data);
$stmt->bindParam(':atualizado_em', $data);

if($stmt->execute()){
  echo "Usuário admin criado com sucesso";
} else {
  echo "Erro ao criar usuário admin";
  echo "<br><br>";
  $error = $stmt->errorInfo();
  echo $error[2];
}

